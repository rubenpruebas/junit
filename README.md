**Se adjunta el proyecto comprimido en zip y una copia de las clases Test extraidas.**
<br/>
<br/>
<br/>
<br/>
![IMAGE_DESCRIPTION](img/GenTestOK.png)
<br/>
Se prueba la clase Gen. Se realizan 40 pruebas totales, todas son existosas, en el siguiente ejemplo forzamos un fallo para verlo.
<br/>
<br/>
<br/>
<br/>
![IMAGE_DESCRIPTION](img/GenTestF.png)
<br/>
Fallo en la prueba numero 8 forzado
<br/>
<br/>
<br/>
<br/>
![IMAGE_DESCRIPTION](img/IndiTestOK.png)
<br/>
Se prueba la clase Individuos. Se realizan 15 pruebas, todas son existosas